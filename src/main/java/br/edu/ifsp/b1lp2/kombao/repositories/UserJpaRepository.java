package br.edu.ifsp.b1lp2.kombao.repositories;

import br.edu.ifsp.b1lp2.kombao.entities.User;
import br.edu.ifsp.b1lp2.kombao.factories.KombaoEntityManagerFactory;

import javax.persistence.EntityManager;
import java.util.Optional;

public class UserJpaRepository implements UserRepository {
    @Override
    public Optional<User> getUser(String email) {
        EntityManager manager = KombaoEntityManagerFactory.createKombaoEntityManager();

        Optional<User> user = manager.createQuery("select user from User user where user.email = :email", User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst();

        return user;
    }

    @Override
    public Optional<User> getUser(String email, String password) {

        EntityManager manager = KombaoEntityManagerFactory.createKombaoEntityManager();

        Optional<User> user = manager.createQuery("select user from User user where user.email = :email and user.password = :password", User.class)
                .setParameter("email", email)
                .setParameter("password", password)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst();

        return user;
    }
}
