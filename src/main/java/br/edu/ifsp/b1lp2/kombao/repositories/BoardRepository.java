package br.edu.ifsp.b1lp2.kombao.repositories;

import java.util.Collection;

import br.edu.ifsp.b1lp2.kombao.entities.Board;
import br.edu.ifsp.b1lp2.kombao.entities.User;

public interface BoardRepository {

	Board getBoard(long id);

	Collection<Board> getAllBoards();

	Collection<Board> getAllActiveBoards();

	Collection<Board> getAllInactiveBoards();

	boolean existsBoardWithName(String boardName);

	void createBoard(String name, User owner);

	void updateBoard(long id, String name);

	void removeBoard(long id);

	void deleteBoardPermanently(long id);

	void restoreBoard(long id);

}