package br.edu.ifsp.b1lp2.kombao.factories;

public enum RepositoryType {
    Memory, SQL, JPA
}
