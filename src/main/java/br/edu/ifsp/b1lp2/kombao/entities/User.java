package br.edu.ifsp.b1lp2.kombao.entities;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Objects;

@Entity
@Table(name="Users")
public class User {

    @Id
    @Column(name="UserId")
    private long userId;

    @Column(name="Name", length = 100, nullable = false)
    private String name;

    @Column(name="Email", length = 50, nullable = false, updatable = false)
    private String email;

    @Column(name="Password", length = 100, nullable = false, columnDefinition = "char")
    private String password;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CreatedAt", insertable=false, updatable=false, nullable=false)
    private Calendar createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="AlteredAt", insertable=false, nullable=false)
    private Calendar alteredAt;

    public long getUserId() { return this.userId; }
    public String getName() { return name; }
    public String getEmail() { return email; }
    public String getPassword() { return password; }
    public Calendar getCreatedAt() {
        return this.createdAt;
    }
    public Calendar getAlteredAt() { return this.alteredAt; }

    public User() {
    }

    public User(long userId, String name, String email, String password){
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public User(String name, String email, String password){
        this.name = name;
        this.email = email;
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userId == user.userId &&
                Objects.equals(name, user.name) &&
                Objects.equals(email, user.email) &&
                Objects.equals(createdAt, user.createdAt) &&
                Objects.equals(alteredAt, user.alteredAt);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userId, name, email, createdAt, alteredAt);
    }
}
