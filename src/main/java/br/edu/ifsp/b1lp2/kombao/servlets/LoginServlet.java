package br.edu.ifsp.b1lp2.kombao.servlets;

import br.edu.ifsp.b1lp2.kombao.entities.User;
import br.edu.ifsp.b1lp2.kombao.factories.RepositoryType;
import br.edu.ifsp.b1lp2.kombao.factories.UserRepositoryFactory;
import br.edu.ifsp.b1lp2.kombao.repositories.UserRepository;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet{

    private UserRepository getRepository() {
        return UserRepositoryFactory.createUserRepositoryFactory(RepositoryType.JPA);
    }

	private static final long serialVersionUID = -5216018965683114549L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/login/index.jsp");
		dispatcher.forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String login = request.getParameter("txtEmail");
		String password = request.getParameter("txtPassword");

		//TODO: Adicionar operação de criptografia

        //Optional é um novo tipo, no Java 8, que permite representar estruturas que podem ou não ter valor
        //Ao invés de eu ficar checando a presença de nulos. Isto reduz a incidencia de nullreferenceexception
        //uma vez que, se estou lidando com um optional, eu preciso verificar o caso vazio.
		Optional<User> user = this.getRepository().getUser(login, password);

		if(user.isPresent()){
			request.getSession().setAttribute("UsuarioLogado", user.get());
            response.sendRedirect("boards");
		} else {
			request.setAttribute("error", "Usuário ou senha inválidos");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/login/index.jsp");
			dispatcher.forward(request, response);
		}
	}
}
