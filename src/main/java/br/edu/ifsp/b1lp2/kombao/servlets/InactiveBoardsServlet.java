package br.edu.ifsp.b1lp2.kombao.servlets;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifsp.b1lp2.kombao.entities.Board;
import br.edu.ifsp.b1lp2.kombao.repositories.BoardJpaRepository;
//import br.edu.ifsp.b1lp2.kombao.repositories.BoardInMemoryRepository;
import br.edu.ifsp.b1lp2.kombao.repositories.BoardRepository;

public class InactiveBoardsServlet extends HttpServlet  {

	private BoardRepository repository;
	protected BoardRepository getRepository() {
		if(repository == null) 
			repository = new BoardJpaRepository();
		
		return repository;
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BoardRepository repository = this.getRepository();
		RequestDispatcher dispatcher = null;
		
		String action = request.getParameter("action") == null? 
				"list" : request.getParameter("action");

		switch(action) {
			case "restore":
				int boardIdToRestore = Integer.parseInt(request.getParameter("id"));
				repository.restoreBoard(boardIdToRestore);
				response.sendRedirect("boards");
				break;
			case "delete":
				int boardIdToRemove = Integer.parseInt(request.getParameter("id"));
				repository.deleteBoardPermanently(boardIdToRemove);
				response.sendRedirect("boards");
				
				break;
			case "list":
				Collection<Board> boards = repository.getAllInactiveBoards();
				request.setAttribute("boards", boards);
				dispatcher = request.getRequestDispatcher("/WEB-INF/views/inactive-boards/list.jsp");
				dispatcher.forward(request, response);
				break;
		}
	}	
	
}