package br.edu.ifsp.b1lp2.kombao.factories;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class KombaoEntityManagerFactory {

    public static EntityManager createKombaoEntityManager() {
        javax.persistence.EntityManagerFactory factory = Persistence.createEntityManagerFactory("kombao");
        EntityManager manager = factory.createEntityManager();
        return manager;
    }
}
