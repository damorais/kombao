package br.edu.ifsp.b1lp2.kombao.factories;

import br.edu.ifsp.b1lp2.kombao.repositories.UserJpaRepository;
import br.edu.ifsp.b1lp2.kombao.repositories.UserRepository;

public class UserRepositoryFactory {

    public static UserRepository createUserRepositoryFactory(RepositoryType type){

        UserRepository repository = null;

        switch(type){
            case SQL:
                throw new IllegalArgumentException("SQL repository not available");
            case Memory:
                throw new IllegalArgumentException("In memory repository not available");
            case JPA:
                repository = new UserJpaRepository();
                break;
            default:
                throw new IllegalArgumentException("Invalid repository type");
        }

        return repository;
    }
}
