package br.edu.ifsp.b1lp2.kombao.repositories;

import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.sql.rowset.spi.TransactionalWriter;

import br.edu.ifsp.b1lp2.kombao.entities.Board;
import br.edu.ifsp.b1lp2.kombao.entities.User;

public class BoardSQLRepository implements BoardRepository {

	private static String DBURL = "jdbc:mariadb://localhost:3306/kombao";
	private static String USERNAME = "root";
	private static String PASSWORD = "";

	private static final String INSERT_STATEMENT = "INSERT INTO Boards(Name) VALUES(?)";
	private static final String UPDATE_STATEMENT = "UPDATE Boards SET Name = ?, AlteredAt = ? WHERE Id = ? AND Active = TRUE";
	private static final String SELECT_ALL_STATEMENT = "SELECT Id, Name, CreatedAt, AlteredAt, Active FROM BOARDS";
	private static final String SELECT_ACTIVES_STATEMENT = "SELECT Id, Name, CreatedAt, AlteredAt, Active FROM BOARDS WHERE Active = TRUE";
	private static final String SELECT_INACTIVES_STATEMENT = "SELECT Id, Name, CreatedAt, AlteredAt, Active FROM BOARDS WHERE Active = FALSE";
	private static final String SELECT_ONE_STATEMENT = "SELECT Id, Name, CreatedAt, AlteredAt, Active FROM BOARDS WHERE Id = ? AND Active = TRUE";
	private static final String INACTIVATE_STATEMENT = "UPDATE Boards SET Active = FALSE WHERE Id = ?";
	private static final String REACTIVATE_STATEMENT = "UPDATE Boards SET Active = TRUE WHERE Id = ?";	
	private static final String DELETE_ONE_STATEMENT = "DELETE FROM Boards WHERE Id = ?";

	public BoardSQLRepository() {
		
	}
	
	@Override
	public Board getBoard(long id) {
		// TODO Implementar
		throw new UnsupportedOperationException("Este método ainda não foi implementado!");
	}

	@Override
	public Collection<Board> getAllBoards() {
		Collection<Board> boards = new ArrayList<>();

		try (Connection conn = (Connection) DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
				PreparedStatement stmt = conn.prepareStatement(SELECT_ALL_STATEMENT);
				ResultSet rs = stmt.executeQuery();) {

			
			while (rs.next()) {

				long id = rs.getLong("Id");
				String name = rs.getString("Name");
				Date createdAt = rs.getTimestamp("CreatedAt"); //Adicionar na classe depois
				Date alteredAt = rs.getTimestamp("AlteredAt"); //Adicionar na classe depois
				boolean active = rs.getBoolean("Active");

				//boards.add(new Board(id, name, active, null));
			}

		} catch (SQLException e) {
			//Tratamento de exceção incorreto! Estou escondendo o erro...
			//Aqui o correto é fazer uma chamada para algum mecanismo de logging para identificar o erro ocorrido.
			System.out.println(e.getMessage());
		}

		return boards;
	}

	@Override
	public Collection<Board> getAllActiveBoards() {
		Collection<Board> boards = new ArrayList<>();

		try (Connection conn = (Connection) DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
				PreparedStatement stmt = conn.prepareStatement(SELECT_ACTIVES_STATEMENT);
				ResultSet rs = stmt.executeQuery();) {

			while (rs.next()) {

				long id = rs.getLong("Id");
				String name = rs.getString("Name");
				Date createdAt = rs.getTimestamp("CreatedAt"); //Adicionar na classe depois
				Date alteredAt = rs.getTimestamp("AlteredAt"); //Adicionar na classe depois
				boolean active = rs.getBoolean("Active");

				//boards.add(new Board(id, name, active));
			}

		} catch (SQLException e) {
			//Tratamento de exceção incorreto! Estou escondendo o erro...
			//Aqui o correto é fazer uma chamada para algum mecanismo de logging para identificar o erro ocorrido.
			System.out.println(e.getMessage());
		}

		return boards;
	}

	@Override
	public Collection<Board> getAllInactiveBoards() {
		// TODO Implementar
		throw new UnsupportedOperationException("Este método ainda não foi implementado!");
	}

	@Override
	public boolean existsBoardWithName(String boardName) {
		// TODO Implementar
		throw new UnsupportedOperationException("Este método ainda não foi implementado!");
	}

	@Override
	public void createBoard(String name, User owner) {
		
		try (Connection conn = (Connection) DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
				PreparedStatement stmt = conn.prepareStatement(INSERT_STATEMENT);) {
				stmt.setString(1, name);
				stmt.executeUpdate();
		
		} catch (SQLException e) {
			//Tratamento de exceção incorreto! Estou escondendo o erro...
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void updateBoard(long id, String name) {
		
		try (Connection conn = (Connection) DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
				PreparedStatement stmt = conn.prepareStatement(UPDATE_STATEMENT);) {
				stmt.setString(1, name);
				stmt.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
				stmt.setLong(3, id);
				int result = stmt.executeUpdate();
				if(result == 0)
					throw new SQLException("Nenhuma modificação efetuada em Quadro.ID: " + id);
		} catch (SQLException e) {
			//Tratamento de exceção incorreto! Estou escondendo o erro...
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void removeBoard(long id) {
		// TODO Implementar
		throw new UnsupportedOperationException("Este método ainda não foi implementado!");
	}

	@Override
	public void deleteBoardPermanently(long id) {
		// TODO Implementar
		throw new UnsupportedOperationException("Este método ainda não foi implementado!");
	}

	@Override
	public void restoreBoard(long id) {
		// TODO Implementar
		throw new UnsupportedOperationException("Este método ainda não foi implementado!");
	}
}
