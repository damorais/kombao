package br.edu.ifsp.b1lp2.kombao.repositories;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;

import javax.persistence.EntityManager;

import br.edu.ifsp.b1lp2.kombao.entities.Board;
import br.edu.ifsp.b1lp2.kombao.entities.User;
import br.edu.ifsp.b1lp2.kombao.factories.KombaoEntityManagerFactory;

public class BoardJpaRepository implements BoardRepository {
	
	@Override
	public Board getBoard(long id) {
		
		EntityManager manager = KombaoEntityManagerFactory.createKombaoEntityManager();
		
		Board board = manager.find(Board.class, id);
		
		manager.close();
	
		return board;
	}

	@Override
	public Collection<Board> getAllBoards() {
		EntityManager manager = KombaoEntityManagerFactory.createKombaoEntityManager();
		
		Collection<Board> boards = manager
				.createQuery("select board from Board board", Board.class)
				.getResultList();
		
		manager.close();
		
		return boards;
	}

	@Override
	public Collection<Board> getAllActiveBoards() {
		EntityManager manager = KombaoEntityManagerFactory.createKombaoEntityManager();
		
		Collection<Board> boards = manager
				.createQuery("select board from Board board where board.isActive = true", Board.class)
				.getResultList();
		
		manager.close();
		
		return boards;
	}

	@Override
	public Collection<Board> getAllInactiveBoards() {
		EntityManager manager = KombaoEntityManagerFactory.createKombaoEntityManager();
		
		Collection<Board> boards = manager
				.createQuery("select board from Board board where board.isActive = false", Board.class)
				.getResultList();
		
		manager.close();
		
		return boards;
	}

	@Override
	public boolean existsBoardWithName(String boardName) {
		EntityManager manager = KombaoEntityManagerFactory.createKombaoEntityManager();
		
		Collection<Board> boards = manager
				.createQuery("select board from Board board where board.name = :boardName", Board.class)
				.setParameter("boardName", boardName)
				.setMaxResults(1)
				.getResultList();
		
		manager.close();
		
		return boards.size() > 0;
	}

	@Override
	public void createBoard(String name, User owner) {
		
		Board board = new Board(name, owner);

		EntityManager manager = KombaoEntityManagerFactory.createKombaoEntityManager();
		
		manager.getTransaction().begin();
		
		manager.persist(board);
		
		manager.getTransaction().commit();
		manager.close();
	}

	@Override
	public void updateBoard(long id, String name) {

		EntityManager manager = KombaoEntityManagerFactory.createKombaoEntityManager();
		
		Board board = manager.find(Board.class, id);
		board.setName(name);
		
		manager.getTransaction().begin();
		manager.persist(board);
		manager.getTransaction().commit();
	
		manager.close();
	}

	@Override
	public void removeBoard(long id) {

		EntityManager manager = KombaoEntityManagerFactory.createKombaoEntityManager();
		
		Board board = manager.find(Board.class, id);
		
		manager.getTransaction().begin();    
		board.setActiveAs(false);
		manager.persist(board);
		
	    manager.getTransaction().commit();  
	    	
		manager.close();
	}

	@Override
	public void deleteBoardPermanently(long id) {
		EntityManager manager = KombaoEntityManagerFactory.createKombaoEntityManager();
		
		Board board = manager.find(Board.class, id);
		
		manager.getTransaction().begin();    
		manager.remove(board);	
	    manager.getTransaction().commit();  
	    	
		manager.close();
	}

	@Override
	public void restoreBoard(long id) {

		EntityManager manager = KombaoEntityManagerFactory.createKombaoEntityManager();
		
		Board board = manager.find(Board.class, id);
		
		String name = board.getName();
		
		if(this.existsBoardWithName(name)) {
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			name = name + " (Restored at:  - " + df.format(Calendar.getInstance().getTime()) + ")";
		}
		
		manager.getTransaction().begin();    
		board.setName(name);
		board.setActiveAs(true);
		
		manager.persist(board);
	    manager.getTransaction().commit();  
	    	
		manager.close();
	}

}
