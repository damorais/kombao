//package br.edu.ifsp.b1lp2.kombao.repositories;
//
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//import java.util.Collection;
//import java.util.HashMap;
//import java.util.Optional;
//import java.util.stream.Collectors;
//
//import br.edu.ifsp.b1lp2.kombao.entities.Board;
//
//public class BoardInMemoryRepository implements BoardRepository {
//	
//	private static HashMap<Long, Board> BoardCollection = new HashMap<>();
//	private static long lastId = 0;
//	
//	public BoardInMemoryRepository() {
//		
//	}
//	
//	/* (non-Javadoc)
//	 * @see br.edu.ifsp.b1lp2.kombao.repositories.BoardRepository#getBoard(int)
//	 */
//	@Override
//	public Board getBoard(long id) {
//		
//		Board board = BoardInMemoryRepository.BoardCollection.get(id);
//		
//		return board;
//	}
//	
//	/* (non-Javadoc)
//	 * @see br.edu.ifsp.b1lp2.kombao.repositories.BoardRepository#getAllBoards()
//	 */
//	@Override
//	public Collection<Board> getAllBoards(){
//		
//		Collection<Board> boards = BoardInMemoryRepository.BoardCollection.values();
//		
//		return boards;
//	}
//	
//	/* (non-Javadoc)
//	 * @see br.edu.ifsp.b1lp2.kombao.repositories.BoardRepository#getAllActiveBoards()
//	 */
//	@Override
//	public Collection<Board> getAllActiveBoards(){
//		
//		return BoardInMemoryRepository.BoardCollection.values()
//				.stream()
//				.filter(board -> board.isActive()).collect(Collectors.toList());
//	}
//	
//	/* (non-Javadoc)
//	 * @see br.edu.ifsp.b1lp2.kombao.repositories.BoardRepository#getAllInactiveBoards()
//	 */
//	@Override
//	public Collection<Board> getAllInactiveBoards(){
//		
//		return BoardInMemoryRepository.BoardCollection.values()
//				.stream()
//				.filter(board -> !board.isActive()).collect(Collectors.toList());
//	}
//	
//	/* (non-Javadoc)
//	 * @see br.edu.ifsp.b1lp2.kombao.repositories.BoardRepository#existsBoardWithName(java.lang.String)
//	 */
//	@Override
//	public boolean existsBoardWithName(String boardName) {
//		//Utilizando a API de Streams do Java 8 + lambda expressions
//		boolean boardsWithSameName = BoardCollection.values()
//				.stream()
//				.anyMatch(board -> board.getName().toLowerCase().equals(boardName.toLowerCase()) && board.isActive());
//		
//		return boardsWithSameName;
//	}
//	
//	/* (non-Javadoc)
//	 * @see br.edu.ifsp.b1lp2.kombao.repositories.BoardRepository#createBoard(java.lang.String)
//	 */
//	@Override
//	public void createBoard(String name) {
//		lastId++;
//		
//		Board newBoard = new Board(lastId, name);
//		
//		BoardInMemoryRepository.BoardCollection.put(lastId,  newBoard);
//	}
//	
//	/* (non-Javadoc)
//	 * @see br.edu.ifsp.b1lp2.kombao.repositories.BoardRepository#updateBoard(int, java.lang.String)
//	 */
//	@Override
//	public void updateBoard(long id, String name) {
//		Board board = new Board(id, name);
//		BoardInMemoryRepository.BoardCollection.put(id, board);
//	}
//	
//	/* (non-Javadoc)
//	 * @see br.edu.ifsp.b1lp2.kombao.repositories.BoardRepository#removeBoard(int)
//	 */
//	@Override
//	public void removeBoard(long id) {
//		Board board = this.getBoard(id);
//		
//		
//		if(board != null) {
//			BoardInMemoryRepository.BoardCollection.put(board.getId(), new Board(board.getId(), board.getName(), false));
//		}
//	}
//	
//	/* (non-Javadoc)
//	 * @see br.edu.ifsp.b1lp2.kombao.repositories.BoardRepository#deleteBoardPermanently(int)
//	 */
//	@Override
//	public void deleteBoardPermanently(long id) {
//		Board board = this.getBoard(id);
//		
//		if(board != null) {
//			BoardInMemoryRepository.BoardCollection.remove(board.getId());
//		}
//	}
//	
//	/* (non-Javadoc)
//	 * @see br.edu.ifsp.b1lp2.kombao.repositories.BoardRepository#restoreBoard(int)
//	 */
//	@Override
//	public void restoreBoard(long id) {
//		Board board = this.getBoard(id);
//		
//		if(board != null && !board.isActive()) {
//			String newName = board.getName() + " (Restored at:  - " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")) + ")";
//			Board reactivatedBoard = new Board(board.getId(), newName);
//			BoardInMemoryRepository.BoardCollection.put(reactivatedBoard.getId(), reactivatedBoard);
//		}
//	}
//}
