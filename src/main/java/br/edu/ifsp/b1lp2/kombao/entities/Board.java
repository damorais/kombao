package br.edu.ifsp.b1lp2.kombao.entities;

import java.util.Calendar;

import javax.persistence.*;

@Entity
@Table(name="Boards")
public class Board {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private long boardId;
	public long getBoardId() {
		return this.boardId;
	}
	
	@Column(name="Name", length=100, nullable=false)
	private String name;

	@Column(name="Active", nullable=false)
	private boolean isActive;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedAt", insertable=false, updatable=false, nullable=false)
	private Calendar CreatedAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="AlteredAt", insertable=false, updatable=true, nullable=false)
	private Calendar AlteredAt;


	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(name="OwnerId")
	private User owner;


    public String getName() {
        return this.name;
    }
    public boolean isActive() {
        return this.isActive;
    }
    public Calendar getCreatedAt() {
        return this.CreatedAt;
    }
	public Calendar getAlteredAt() {
		return this.AlteredAt;
	}
    public User getOwner(){
        return this.owner;
    }

	public Board() {
		
	}
	
	public Board(String name, User owner) {
		this.name = name;
        this.owner = owner;
        this.isActive = true;
	}
	
	public Board(long boardId, String name, User owner) {
		this.boardId = boardId;
		this.name = name;
        this.owner = owner;
		this.isActive = true;
	}

    //Defini este nome no método para não caracterizar como uma propriedade (getter / setter)
    public void setActiveAs(boolean newStatus) {
        this.isActive = newStatus;
    }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + this.boardId);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Board other = (Board) obj;
		if (boardId != other.boardId)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@PreUpdate
	protected void onAltered() {
	    this.AlteredAt = Calendar.getInstance();
	}	
	
	@Override
	public String toString() {
		return "Board [Id=" + boardId + ", name=" + name + "]";
	}


	public void setName(String name) {
		this.name = name;
	}
}
