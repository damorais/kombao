package br.edu.ifsp.b1lp2.kombao.repositories;

import br.edu.ifsp.b1lp2.kombao.entities.User;

import java.util.Optional;

public interface UserRepository {

    Optional<User> getUser(String email);

    Optional<User> getUser(String email, String password);


}
