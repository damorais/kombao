package br.edu.ifsp.b1lp2.kombao.servlets;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifsp.b1lp2.kombao.entities.Board;
//import br.edu.ifsp.b1lp2.kombao.repositories.BoardInMemoryRepository;
import br.edu.ifsp.b1lp2.kombao.entities.User;
import br.edu.ifsp.b1lp2.kombao.repositories.BoardJpaRepository;
import br.edu.ifsp.b1lp2.kombao.repositories.BoardRepository;
import br.edu.ifsp.b1lp2.kombao.repositories.BoardSQLRepository;


public class BoardsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private BoardRepository repository;
	protected BoardRepository getRepository() {
		if(repository == null) 
			repository = new BoardJpaRepository();
			//repository = new BoardSQLRepository();//new BoardInMemoryRepository();
		
		return repository;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		RequestDispatcher dispatcher = null;
		
		String action = request.getParameter("action") == null? 
				"list" : request.getParameter("action");
		
		switch(action) {
			case "show":
				int boardId = Integer.parseInt(request.getParameter("id"));
				
				Board board = this.getRepository().getBoard(boardId);
				request.setAttribute("board", board);
				
				dispatcher = request.getRequestDispatcher("/WEB-INF/views/boards/show.jsp");
				dispatcher.forward(request, response);
				break;
			case "delete":
				int boardIdToRemove = Integer.parseInt(request.getParameter("id"));
				this.getRepository().removeBoard(boardIdToRemove);
				response.sendRedirect("boards");
				
				break;
			case "list":
				
				this.listBoards("", request, response);
				
				break;
		}

	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String boardName = request.getParameter("txtBoardName");
		
		if(!this.getRepository().existsBoardWithName(boardName)) {

			User userAuthenticated = (User) request.getSession().getAttribute("UsuarioLogado");
			
			this.getRepository().createBoard(boardName, userAuthenticated);
			response.sendRedirect("boards");
			
		} else {
			String errorMessage = "Já existe um quadro com o nome: " + boardName + ". Por favor, escolha outro nome.";
			
			this.listBoards(errorMessage, request, response);
		}
	}
	
	private void listBoards(String errorMessage, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(!errorMessage.isEmpty()) {
			request.setAttribute("errorMessage", errorMessage);
		}
			
		
		Collection<Board> boards = this.getRepository().getAllActiveBoards();
		request.setAttribute("boards", boards);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/boards/list.jsp");
		dispatcher.forward(request, response);
	}
}
