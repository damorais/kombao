<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="kombao" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<kombao:bootstrapCSS></kombao:bootstrapCSS>
<title>Meus Quadros</title>
</head>
<body class="container-fluid">
	<h1>Meus Quadros</h1>
	
	<div class="row">
		<c:choose>
			<c:when test="${fn:length(boards) > 0 }">
				<c:forEach var="board" items="${ boards }">
					<div class="card" style="width: 18rem;">
					  <div class="card-body">
					    <h5 class="card-title">
					     	<a href="inactive-boards?id=${ board.boardId }&action=restore" class="card-link">Restaurar ${board.name}</a>
					    </h5>
					   	<a href="inactive-boards?id=${ board.boardId }&action=delete" class="card-link"> Remover permanentemente</a>
					  </div>
					</div>
				</c:forEach>	 
			</c:when>
			<c:otherwise>
				<h2>Sua lixeira está vazia.</h2>
			</c:otherwise>
		</c:choose>
	</div>
	
	<kombao:bootstrapJS></kombao:bootstrapJS>
</body>
</html>
