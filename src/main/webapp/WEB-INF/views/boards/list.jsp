<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="kombao" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<kombao:bootstrapCSS></kombao:bootstrapCSS>
<kombao:bootstrapJS></kombao:bootstrapJS>
<title>Meus Quadros</title>
</head>
<body class="container-fluid">
	<h1>Meus Quadros</h1>
	
	<div class="row">
		<c:if test="${not empty errorMessage}">
			<div class="alert alert-danger" role="alert">
  				${errorMessage}
			</div>
		</c:if>
	
		<form class="form" method="POST">
			<kombao:inputtext label="Novo Quadro" fieldId="txtBoardName"></kombao:inputtext>		
			<button type="submit" class="btn btn-success">Criar...</button>
		</form>
	</div>
	<div class="row">
		<c:choose>
			<c:when test="${fn:length(boards) > 0 }">
				<c:forEach var="board" items="${ boards }">
					<div class="card" style="width: 18rem;">
					  <div class="card-body">
					    <h5 class="card-title">
					     	<a href="boards?id=${ board.boardId }&action=show" class="card-link">${board.name}</a>
							Proprietário: ${board.owner.name}
					    </h5>
					    <a href="boards?id=${ board.boardId }&action=delete" class="card-link"> remove board </a>
					  </div>
					</div>
				</c:forEach>	 
			</c:when>
			<c:otherwise>
				<h2>Não há quadros cadastrados!!!!</h2>
			</c:otherwise>
		</c:choose>
	</div>
	
	<a href="inactive-boards"> Ver lixeira </a>
	

</body>
</html>
