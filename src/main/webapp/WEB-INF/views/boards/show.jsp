<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="kombao" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<kombao:bootstrapCSS></kombao:bootstrapCSS>
<title>Insert title here</title>
</head>
<body>
	<h1> ${ board.boardId } - ${ board.name }</h1>

	<kombao:bootstrapJS></kombao:bootstrapJS>

</body>
</html>