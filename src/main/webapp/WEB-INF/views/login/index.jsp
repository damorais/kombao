<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="kombao" %>

<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'>
<meta name='viewport'
	content='width=device-width, initial-scale=1, shrink-to-fit=no'>
	<kombao:bootstrapCSS></kombao:bootstrapCSS>

</head>
<body>
	<div class="container">
		<form class="form-horizontal" role="form" method="POST"
			action="login">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<h2>Acessar aplicação</h2>
					<hr>
				</div>
			</div>
			<c:if test="${not empty error}">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
					  ${ error }
					</div>
				</div>
			</div>
			</c:if>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="form-group has-danger">
						<label class="sr-only" for="txtEmail">Endereço de Email</label>
						<div class="input-group mb-2 mr-sm-2 mb-sm-0">
							<div class="input-group-addon" style="width: 2.6rem">
								<i class="fa fa-at"></i>
							</div>
							<input type="text" name="txtEmail" class="form-control" id="txtEmail"
								placeholder="Seu email" required autofocus>
						</div>
					</div>
				</div>
			
			</div>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="sr-only" for="txtPassword">Senha</label>
						<div class="input-group mb-2 mr-sm-2 mb-sm-0">
							<div class="input-group-addon" style="width: 2.6rem">
								<i class="fa fa-key"></i>
							</div>
							<input type="password" name="txtPassword" class="form-control"
								id="txtPassword" placeholder="Sua senha" required>
						</div>
					</div>
				</div>
				
			</div>
			<!-- <div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6" style="padding-top: .35rem">
					<div class="form-check mb-2 mr-sm-2 mb-sm-0">
						<label class="form-check-label"> <input
							class="form-check-input" name="remember" type="checkbox">
							<span style="padding-bottom: .15rem">Lembrar senha</span>
						</label>
					</div>
				</div>
			</div> -->
			<div class="row" style="padding-top: 1rem">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<button type="submit" class="btn btn-success">
						<i class="fa fa-sign-in"></i> Acessar
					</button>
					<!-- <a class="btn btn-link" href="/password/reset">Esqueceu sua senha?</a> -->
				</div>
			</div>
		</form>
	</div>
<kombao:bootstrapJS></kombao:bootstrapJS>
</body>
</html>
