<%@ tag language="java" pageEncoding="UTF-8"%>

<%@ attribute name="label" required="true" %>
<%@ attribute name="fieldId" required="true" %>

<div class="form-group">
    <label for="txtBoardName">${label}</label>
    <input type="text" class="form-control" id="${fieldId}" name="${fieldId}">
</div>